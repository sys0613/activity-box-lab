## activity-box-lab

### 介绍

随机试炼的活动，包括编译软件包ABS、测试T-One等任务。

该活动以拆盲盒的形式，随机为你发放一个任务，你可以领取也可以重拆。

### 活动的操作步骤

详见[随机试炼的帮助文档](https://gitee.com/anolis-challenge/summer2022/tree/master/%E9%9A%8F%E6%9C%BA%E8%AF%95%E7%82%BC)。

注意：完成编译或者测试后，要在本仓库中提交PR。否则，无法获得贡献值。
